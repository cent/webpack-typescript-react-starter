import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.css';

const App = () => (
  <h1>React App!</h1>
);

ReactDOM.render(<App/>, document.getElementById('root'));
