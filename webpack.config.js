/* eslint-disable global-require  */
/* eslint-disable import/no-dynamic-require  */
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
// const CompressionPlugin = require("compression-webpack-plugin");
const AssetsPlugin = require('assets-webpack-plugin');
const { CheckerPlugin } = require('awesome-typescript-loader');


const devMode = process.env.NODE_ENV === 'development';

const src = path.join(__dirname, 'src');


const config = {
  mode: devMode ? 'development' : 'production',
  devtool: devMode ? 'source-map' : false,

  // output system
  output: {
    path: path.join(src, '../dist/bundle'),
    publicPath: '/static/bundle/',
    filename: devMode ? '[name].js' : '[name].[chunkhash].js',
    chunkFilename: devMode ? '[id].js' : '[name].[id].[chunkhash].js',
  },

  context: __dirname,

  entry: {
    // _hot_: 'webpack/hot/only-dev-server',
    app: path.join(src, 'index.tsx'),
    vendors: Object.keys(require(path.join(__dirname, '/package')).dependencies),
  },

  // resolves modules
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
    // modulesDirectories: ['node_modules'],
    alias: {
      _app: path.join(src),
      // _img: path.join(src, 'assets', 'img'),
    },
  },
  module: {
    rules: [
      {
        test: /\.(t|j)sx?$/,
        use: { loader: 'awesome-typescript-loader' },
        exclude: /(node_modules|bower_components)/,
      },
      {
        enforce: 'pre',
        test: /\.tsx?$/,
        loader: 'tslint-loader',
        exclude: /node_modules/,
        options: {
          failOnHint: true,
          configuration: require('./tslint.json'),
        },
      },
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader',
        exclude: /(node_modules|bower_components)/,
      },
      {
        test: /\.(ttf|eot|woff(2)?|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/i,
        loaders: ['url-loader?limit=10000&name=[name].[ext]'], // name=[name]-[hash].[ext]
      },
      {
        test: /\.(png|ico|jpg|jpeg|gif|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/i,
        loaders: ['url-loader?limit=10000&name=[name].[ext]'],
      },
      {
        test: /\.(s?[ac]ss)|(less)$/,
        use: [
          devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'less-loader',
          'sass-loader',
        ],
      },
    ],
  },

  // post css
  // postcss: [autoprefixer({ browsers: ['last 5 versions'] })],

  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new MiniCssExtractPlugin({
      filename: devMode ? '[name].css' : '[name].[hash].css',
      chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
    }),
    // new CompressionPlugin({ // "compression-webpack-plugin": "^1.1.11",
    //   test: /\.js/
    // }),
    new AssetsPlugin({ path: path.join(__dirname, 'dist/'), filename: 'assets.json', prettyPrint: true }),

    new HtmlWebpackPlugin({
      template: 'src/index.html',
      appMountId: 'root',
    }),
  ],

  optimization: {
    namedModules: true, // NamedModulesPlugin()
    splitChunks: { // CommonsChunkPlugin()
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
        },
      },
    },
    noEmitOnErrors: true, // NoEmitOnErrorsPlugin
    concatenateModules: true, // ModuleConcatenationPlugin
    minimizer: [
      new UglifyJsPlugin({
        uglifyOptions: {
          ecma: 6,
          warnings: false,
          compress: {
            // ecma: 6,
            drop_console: true,
          },
          // mangle: {
          //   keep_fnames: true,
          // },
          output: {
            ecma: 6,
            comments: false,
            beautify: false,
          },
          ie8: false,
        },
        sourceMap: false,
        parallel: true,
      }),
    ],
  },
};

if (devMode) {
  config.plugins.push(new CheckerPlugin());
  config.plugins.push(new webpack.HotModuleReplacementPlugin());

  config.devServer = {
    port: 8080,
    contentBase: config.output.path,
    publicPath: config.output.publicPath,
    inline: true,
    hot: true,
    // hotOnly: true,
    progress: true,
    historyApiFallback: true,
    proxy: {
      '/api/*': 'http://localhost:8000',
    },
  };
}

module.exports = config;
